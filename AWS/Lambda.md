At the Brisbane AWS Meetup on Wednesday 19th of October, one presentation was around developing for AWS Lambda locally.

This can be done using a few tools that can be used to simulate a few of the AWS APIs:

- https://github.com/spulec/moto - Python library allowing mocking of boto calls
- https://github.com/trayio/dynamodb-local - dynamo db local docker image
- https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html - dynamodb_local
- https://github.com/serverless/serverless - serverless framework
- https://github.com/ticketsolutionsptyltd/serverless-offline-example - Serverless framework offline example
-
The presentation was around getting developers up and running quickly with a working dev environment locally that closely resembles production.
Also how to preseed data into local environments.

Questions were asked about how well local simulations of AWS services interacting between each other works and the response was that this scenario is lacking currently, 
but dev code interacting with AWS APIs individually/directly was working quite well for a lot of the APIS.
Some APIs are missing but it's opensource so go write it.
